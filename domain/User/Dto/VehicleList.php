<?php

namespace Domain\User\Dto;

class VehicleList
{
    public int $userId;
    public string $userName;
    public string $userEmail;

    private array $vehicles = [];

    public function getVehicles(): array
    {
        return $this->vehicles;
    }

    public function addVehicle(Vehicle $vehicle): VehicleList
    {
        $this->vehicles[] = $vehicle;
        return $this;
    }
}
