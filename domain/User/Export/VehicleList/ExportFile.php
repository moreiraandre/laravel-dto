<?php

namespace Domain\User\Export\VehicleList;

use App\Models\User;
use Domain\User\Dto\Vehicle as VehicleDto;
use Domain\User\Dto\VehicleList as VehicleListDto;
use Illuminate\Support\Facades\Storage;
use Domain\User\Export\VehicleList\Formats\{FormatInterface, Json, Xml};

class ExportFile
{
    public const JSON = 1;
    public const XML = 2;

    /**
     * @param int $format
     * @param int $userId
     * @return string Generated file name
     * @throws \Exception
     */
    public function export(int $format, int $userId): string
    {
        switch ($format) {
            case self::JSON:
                $objectFormat = new Json();
                break;
            case self::XML:
                $objectFormat = new Xml();
                break;
            default:
                throw new \Exception('Format not found.');
        }

        return $this->saveFile($userId, $objectFormat, $format);
    }

    private function saveFile(int $userId, FormatInterface $objectFormat, string $format): string
    {
        $vehicleList = $this->prepareVehicleList($userId);
        $exportContent = $objectFormat->export($vehicleList);
        $fileExtension = $this->prepareFileExtension($format);
        $fileName = "user/vehicle-list/user-$userId.$fileExtension";

        Storage::disk('public')->put($fileName, $exportContent);

        return $fileName;
    }

    private function prepareFileExtension(int $format): string
    {
        $fileExtension = '';
        switch ($format) {
            case self::JSON:
                $fileExtension = 'json';
                break;
            case self::XML:
                $fileExtension = 'xml';
                break;
        }

        return $fileExtension;
    }

    private function prepareVehicleList(int $userId): VehicleListDto
    {
        $user = User::findOrFail($userId);
        $vehicleListDto = new VehicleListDto();

        $vehicleListDto->userId = $user->getKey();
        $vehicleListDto->userName = $user->name;
        $vehicleListDto->userEmail = $user->email;

        $vehicles = $user->vehicles;
        foreach ($vehicles as $vehicle) {
            $vehicleDto = new VehicleDto();

            $vehicleDto->id = $vehicle->getKey();
            $vehicleDto->plate = $vehicle->plate;
            $vehicleDto->color = $vehicle->color;
            $vehicleDto->manufacturingDate = $vehicle->manufacturing_date;

            $vehicleListDto->addVehicle($vehicleDto);
        }

        return $vehicleListDto;
    }
}
