<?php

namespace Domain\User\Dto;

use Illuminate\Support\Carbon;

class Vehicle
{
    public int $id;
    public string $plate;
    public string $color;
    public Carbon $manufacturingDate;
}
