<?php

namespace Domain\User\Export\VehicleList\Formats;

use Domain\User\Dto\Vehicle as VehicleDto;
use Domain\User\Dto\VehicleList as VehicleListDto;

class Xml implements FormatInterface
{
    public function export(VehicleListDto $vehicleListDto): string
    {
        $vehicles = $this->prepareVehicles($vehicleListDto->getVehicles());

        $xml = "<userId>$vehicleListDto->userId</userId>";
        $xml .= "<userName>$vehicleListDto->userName</userName>";
        $xml .= "<userEmail>$vehicleListDto->userEmail</userEmail>";
        $xml .= "<vehicles>$vehicles</vehicles>";

        return "<VehicleList>$xml</VehicleList>";
    }

    private function prepareVehicles(array $vehiclesItems): string
    {
        $vehicles = '';
        /** @var VehicleDto $vehicleDto */
        foreach ($vehiclesItems as $vehicleDto) {
            $vehicleXml = "<id>$vehicleDto->id</id>";
            $vehicleXml .= "<plate>$vehicleDto->plate</plate>";
            $vehicleXml .= "<color>$vehicleDto->color</color>";
            $vehicleXml .= "<manufacturingDate>{$vehicleDto->manufacturingDate->toDateString()}</manufacturingDate>";

            $vehicles .= "<vehicleDto>$vehicleXml</vehicleDto>";
        }

        return $vehicles;
    }
}
