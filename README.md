# Laravel com DTO
A intenção desse projeto é estudar o padrão [DTO](https://pt.wikipedia.org/wiki/Objeto_de_Transfer%C3%AAncia_de_Dados) 
com o framework Laravel (PHP), desenvolvendo uma aplicação para exportar mais de um tipo de arquivo com o mesmo DTO de 
entrada.

# Pré-requisitos
- PHP7.4+
- Sqlite

# Instalação
## Faça a cópia da configuração
```bash
cp .env.example .env
```

## Crie o banco Sqlite
```bash
touch database/database.sqlite
```

## Execute as migrations com a seed
Esse comando criará as tabelas no banco de dados e irá popular as tabelas `users` e `vehicles` com dados fake.
```bash
php artisan migrate --seed
```

## Utilize o comando Artisan para criar o arquivo
```bash
php artisan export:vehicle-list 10
```
> `10` é o id do usuário, verifique quais ids criados na tabela `users`.

# Lógica
A lógica e as estruturas estão na pasta `domain`.
