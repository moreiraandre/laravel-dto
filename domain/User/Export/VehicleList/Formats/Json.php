<?php

namespace Domain\User\Export\VehicleList\Formats;

use Domain\User\Dto\Vehicle as VehicleDto;
use Domain\User\Dto\VehicleList as VehicleListDto;

class Json implements FormatInterface
{
    public function export(VehicleListDto $vehicleListDto): string
    {
        $vehicles = $this->prepareVehicles($vehicleListDto->getVehicles());

        return json_encode(
            [
                'user_id' => $vehicleListDto->userId,
                'user_name' => $vehicleListDto->userName,
                'user_email' => $vehicleListDto->userEmail,
                'vehicles' => $vehicles,
            ]
        );
    }

    private function prepareVehicles(array $vehiclesList): array
    {
        $vehicles = [];
        /** @var VehicleDto $vehicleDto */
        foreach ($vehiclesList as $vehicleDto) {
            $vehicles[] = [
                'id' => $vehicleDto->id,
                'plate' => $vehicleDto->plate,
                'color' => $vehicleDto->color,
                'manufacturing_date' => $vehicleDto->manufacturingDate->toDateString(),
            ];
        }

        return $vehicles;
    }
}
