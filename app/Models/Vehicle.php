<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Vehicle extends Model
{
    use HasFactory;

    protected $dates = ['manufacturing_date'];

    public function setPlateAttribute($plate)
    {
        $this->attributes['plate'] = str_replace(' ', '', $plate);
    }
}
