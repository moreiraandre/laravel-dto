<?php

namespace App\Console\Commands;

use Domain\User\Export\VehicleList\ExportFile;
use Illuminate\Console\Command;

class ExportVehicleList extends Command
{
    /**
     * The name and signature of the console command.
     * @var string
     */
    protected $signature = 'export:vehicle-list {user : Id of user}';

    /**
     * The console command description.
     * @var string
     */
    protected $description = 'Export vehicle list by user';

    /**
     * Execute the console command.
     * @return int
     */
    public function handle(ExportFile $exportFile)
    {
        $format = $this->choice('Select the format please', ['Json', 'Xml']);
        switch ($format) {
            case 'Json':
                $format = ExportFile::JSON;
                break;
            case 'Xml':
                $format = ExportFile::XML;
                break;
        }

        $fileName = $exportFile->export($format, $this->argument('user'));
        $this->info('Generated file: ' . storage_path('public/' . $fileName));

        return Command::SUCCESS;
    }
}
