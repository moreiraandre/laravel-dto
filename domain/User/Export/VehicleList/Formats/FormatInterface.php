<?php

namespace Domain\User\Export\VehicleList\Formats;

use Domain\User\Dto\VehicleList as VehicleListDto;

interface FormatInterface
{
    public function export(VehicleListDto $vehicleListDto): string;
}
