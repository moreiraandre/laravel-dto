<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class VehicleFactory extends Factory
{
    /**
     * Define the model's default state.
     * @return array
     */
    public function definition()
    {
        return [
            'user_id' => User::inRandomOrder()->first()->getKey(),
            'plate' => $this->faker->jpjNumberPlate(),
            'color' => $this->faker->safeColorName(),
            'manufacturing_date' => now()->subYears(rand(1, 5)),
        ];
    }
}
